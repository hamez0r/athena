package edu.crs.athena;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import edu.crs.athena.SendCoordinatesService;

public class SendCoordinatesAlarmScheduler {
    private Context mContext;

    public SendCoordinatesAlarmScheduler(Context context) {
        mContext = context;
        Log.i("Scheduler", "Scheduler Context: " + mContext.toString());
    }

    public void ScheduleRepeteadAlarm(long triggerInterval) {
        // Create an intent that will execute SendCoordinatesAlarmReceiver,
        // who in turn will execute SendCoordinatesService
        Intent intent = new Intent(mContext, SendCoordinatesAlarmReceiver.class);

        // As per PendingIntent documentation:
        // If you only need one PendingIntent active at a time for any of the Intents you will use,
        // then you can alternatively use the flags FLAG_CANCEL_CURRENT or FLAG_UPDATE_CURRENT
        // to either cancel or modify whatever current PendingIntent is associated with the Intent
        // you are supplying.
        PendingIntent pendingIntent =
                PendingIntent.getBroadcast(mContext, SendCoordinatesAlarmReceiver.REQUEST_CODE,
                        intent, PendingIntent.FLAG_CANCEL_CURRENT);

        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

        // Trigger immediately.
        // AlarmManager.setRepeating(...) will trigger if triggerTimeNow was in the past.
        long triggerTimeNow = System.currentTimeMillis();
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, triggerTimeNow,
                triggerInterval, pendingIntent);

        Log.i("Scheduler", "Alarm has been scheduled");
    }
}

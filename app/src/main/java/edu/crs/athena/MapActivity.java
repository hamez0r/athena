// -------------------------------------------------------------------------------------------------
// Notes
//
//
// Big discussion over Activity`s lifecycle.
// Also, when all the implementing methods are called, and why
// -------------------------------------------------------------------------------------------------

package edu.crs.athena;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;


public class MapActivity extends Activity implements OnMapReadyCallback {

    private static final int ALARM_REPEAT_INTERVAL = 2000; // in milliseconds
    private List<UserLocation> mLocations; // locations to display on map
    private Handler mHandler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Content View is where information can be exposed to user, or get input from.
        setContentView(R.layout.activity_map);

        // Register with the Alarm system service to make the device send its coordinates.
        // This registration is also done after boot (automatically).
        this.setCoordinateSendingAlarm();
    }

    private void setCoordinateSendingAlarm() {
        SendCoordinatesAlarmScheduler scheduler = new SendCoordinatesAlarmScheduler(this);
        scheduler.ScheduleRepeteadAlarm(ALARM_REPEAT_INTERVAL);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // new LocationsRetrievalTask().execute();
        new Thread(new UpdateTask(mHandler)).start();
    }

    private void setUpMap() {
        // onMapReady will be called when the async operation ends.
        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();

        // This is called after the locations of other devices are retrieved from the server.
        // So at this point we have a map and a lot of locations to put on it.

        ListIterator<UserLocation> locations = mLocations.listIterator();
        while(locations.hasNext()) {
            UserLocation entry = locations.next();

            String userName = entry.getName();
            LatLng location = entry.getLocation();

            googleMap.addMarker(new MarkerOptions().position(location).title(userName));
        }
    }

    private class UserLocation {
        private String mName;
        private LatLng mLocation;

        UserLocation(String name, LatLng location) {
            this.setName(name);
            this.setLocation(location);
        }

        public void setName(String name) { mName = name; }
        public void setLocation(LatLng location) { mLocation = location; }

        public String getName() { return mName; }
        public LatLng getLocation() { return mLocation; }
    }

    private class LocationsRetrievalTask extends AsyncTask<Void, Void, List<UserLocation>> {

        @Override
        protected List<UserLocation> doInBackground(Void... params) {
            List<UserLocation> locationsFromServer = new LinkedList<>();

            SharedPreferences preferences = getSharedPreferences("user_name", MODE_PRIVATE);
            String defaultUserName = getResources().getString(R.string.default_user_name);
            String userName = preferences.getString("username", defaultUserName);

            URL url = null;
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();
            try {
                url = new URL("http://37.139.6.168:3000/locations?username=" + userName);
                urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");

                InputStream response = urlConnection.getInputStream();
                BufferedReader responseReader = new BufferedReader(new InputStreamReader(response));


                String line;
                while ((line = responseReader.readLine()) != null)
                    result.append(line);


                System.out.println("Server reply: " + result.toString());
            } catch (Exception e) {
                System.out.println("Something went wrong when getting locations: " + e.toString());
            }

            try {
                JSONObject responseJSON = new JSONObject(result.toString());
                JSONArray positions = responseJSON.getJSONArray("positions");

                for (int i = 0; i < positions.length(); ++i) {
                    JSONObject data = positions.getJSONObject(i);
                    String user = data.getString("user");
                    JSONObject position = data.getJSONObject("pos");

                    double lat = position.getDouble("lat");
                    double lng = position.getDouble("lng");

                    LatLng location = new LatLng(lat, lng);

                    locationsFromServer.add(new UserLocation(user, location));
                }
            } catch (JSONException jsonEx) {
                System.out.println("JSON exception: " + jsonEx.toString());
            }

            return locationsFromServer;
        }

        @Override
        protected void onPostExecute(List<UserLocation> locations) {
            mLocations = locations;
            setUpMap();
        }
    }

    private class UpdateTask implements Runnable {
        private final int SLEEP_INTERVAL = 2000; // milliseconds
        private Handler mHandler;

        public UpdateTask(Handler handler) {
            mHandler = handler;
        }

        @Override
        public void run() {
            while(true) {
                try {
                    Thread.sleep(SLEEP_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                mHandler.post(new Runnable() {
                    public void run() {
                        new LocationsRetrievalTask().execute();
                    }
                });
            }
        }
    }
}

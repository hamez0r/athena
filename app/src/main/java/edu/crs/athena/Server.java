import java.net.*;
import java.io.*;

public class Server {


	public static void main(String[] args) {

		ServerSocket mainSocket = null;
		Socket clientSocket = null;
		// read and write from the socket
		BufferedReader clientIn = null;
		PrintWriter serverOut = null;
		int port = 4567;
		try {
			mainSocket = new ServerSocket(port);

			clientSocket = mainSocket.accept(); // waits for the client to connect 
			// read and write from the socket
			clientIn = new BufferedReader(
				new InputStreamReader (clientSocket.getInputStream()));
			serverOut = new PrintWriter(clientSocket.getOutputStream(), true);
		}
		catch (IOException e) {
			System.out.println("IO Exception !!!");
		}

		String input = "I am sad";
		while (true) {
			try {
				input = clientIn.readLine();	
			} catch(IOException e) {
				System.out.println("Exception caught when trying to read input");
				continue;
			}			
			System.out.println("I received a message: ");
			System.out.println(input);

			// send basic reply
			serverOut.println("Hi, Client!");
		}
	}
}
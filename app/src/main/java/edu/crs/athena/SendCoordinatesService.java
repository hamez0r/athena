package edu.crs.athena;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class SendCoordinatesService extends IntentService implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static String urlString = "http://37.139.6.168:3000/update";

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;


    private static final String LOCATIONS_API = "Locations API: ";

    public SendCoordinatesService(){
        super("SendCoordinatesService");

    }

    private String getCoordinates() {
        return new String();
    }

    /* Reads SharedPreferences file.
       If no username is present, returns default username.
     */
    private String getUserName() {
        SharedPreferences preferences = this.getSharedPreferences("user_name", MODE_PRIVATE);
        String defaultUserName = getResources().getString(R.string.default_user_name);
        String userName = preferences.getString("username", defaultUserName);
        Log.i("Username: ", userName);
        return userName;
    }

    private void initializeGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this, this, this)
                .addApi(LocationServices.API)
                .build();
    }

    private void disconnectGoogleApiClient() {
        this.mGoogleApiClient.disconnect();
    }

    /* Initializes GoogleApiClient, gets the location
       connects to server and sends location.
    */
    @Override
    protected void onHandleIntent(Intent intent){

        this.initializeGoogleApiClient();

        // Wait for the connection to Locations API
        ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(5, TimeUnit.SECONDS);
        if(!connectionResult.isSuccess()) {
            Log.i(LOCATIONS_API, "Failed connection to Locations API");
            return;
        } else {
            Log.i(LOCATIONS_API, "Successfully connected to Locations API");
        }

        // By this time, onConnected method should have been called (I think),
        // so we have a location to send.

        Log.i("SendCoordinatesService", "Sending coordinates...");

        JSONObject position = new JSONObject();
        JSONObject postBody = new JSONObject();
        try {
            postBody.put("username", this.getUserName());

            position.put("lat", mLocation.getLatitude());
            position.put("lng", mLocation.getLongitude());

            postBody.put("position", position);
        } catch (JSONException ex) {
            System.out.println ("Couldn't set username...");
        }

        this.sendGeoLocation(postBody.toString());
        this.disconnectGoogleApiClient();
    }

    private void sendGeoLocation(String location) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.setRequestMethod("PUT");
            urlConnection.setRequestProperty("Content-Type", "application/json");

            OutputStream body = urlConnection.getOutputStream();
            body.write(location.getBytes());

            urlConnection.getInputStream(); // This fires the sending of request
        } catch (Exception e) {
            System.out.println("Something went wrong when sending location: " + e.toString());
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(LOCATIONS_API, "Connected to Locations API");

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        Log.i(LOCATIONS_API, "My location is " + mLocation.toString());
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(LOCATIONS_API, "Connection to Locations API suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(LOCATIONS_API, "Failed to connect to Locations API");
        Log.i(LOCATIONS_API, connectionResult.getErrorMessage());
    }
}

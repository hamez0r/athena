package edu.crs.athena;

import java.net.*;
import java.io.*;

public class Client {

	public static void main (String[] args) {

		Socket socket = null;
		// read and write from the socket
		BufferedReader in = null;
		PrintWriter out = null;

		InetAddress address = null;
		try {
			address = Inet6Address.getByName("2a02:2f0e:d280:1083:f0ad:9a4b:c91a:5784");	
			System.out.println(address.toString());
		} catch (UnknownHostException e) {
			System.out.println("UnknownHostException !!!");
		}		

		try {
			String targetHostName = "localhost";
			int    port 		  = 4567;
			socket = new Socket(address, port, null, 0);

			// to send, use the Socket`s OutputStream
			// true = auto flush
			out = new PrintWriter (socket.getOutputStream(), true);

			// to read the received information from the Socket`s InputStream
			in = new BufferedReader(new InputStreamReader (socket.getInputStream()));
		}
		catch (UnknownHostException e) {
			System.out.println("Unknown host !!!");
		}
		catch (IOException e) {
			System.out.println("Unknown exception !!!");
		}
		// The connection has finished setting up

		// send something
		System.out.println("I will now send\n\n");
		out.println("Hi, Server!");

		

		/**********************************************************************/
		// Might need to add try-catch
		/**********************************************************************/

		// get the reply
		System.out.println("I received a message: ");
		String response = "I don`t have info";
		try {
			response = in.readLine(); // waits until the server replies
		}
		catch (IOException e) {
			System.out.println("Exception caught when trying to read reply");
		}
		System.out.println(response);

		// resources are cleared because of the try statement
	}

}
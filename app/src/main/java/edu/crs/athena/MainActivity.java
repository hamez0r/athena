package edu.crs.athena;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {

    private Handler mHandler = new Handler();

    String mUsername = "";
    static String mBaseURL = "http://37.139.6.168:3000/";

    // Friend Requests list
    List<HashMap<String, String>> mFriendRequestsData = new ArrayList<>();
    static int mFriendRequestLayout = R.layout.friend_request_list_item;
    static String[] mFriendRequestsColumns = { "FriendName" };
    static int[] mFriendRequestsTargetViews = { R.id.friend_name };
    SimpleAdapter mFriendRequestsListAdapter;

    // Friends list
    List<HashMap<String, String>> mFriendsData = new ArrayList<>();
    static int mFriendsLayout = R.layout.friend_list_item;
    static String[] mFriendsColumns = { "FriendName" };
    static int[] mFriendsTargetViews = { R.id.existing_friend_name };
    SimpleAdapter mFriendsListAdapter;

    private class UpdateTask implements Runnable {
        private final int SLEEP_INTERVAL = 2000; // milliseconds
        private Handler mHandler;

        public UpdateTask(Handler handler) {
            mHandler = handler;
        }

        @Override
        public void run() {
            while(true) {
                try {
                    Thread.sleep(SLEEP_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                mHandler.post(new Runnable() {
                    public void run() {
                        try {
                            URL url = new URL("http://37.139.6.168:3000/pending_requests?username=" + mUsername);
                            new GetNamesList(mFriendRequestsData, mFriendRequestsColumns, mFriendRequestsListAdapter).execute(url);
                        } catch (Exception e) {
                            System.out.println(e.toString());
                        }

                        try {
                            URL url = new URL("http://37.139.6.168:3000/friends?username=" + mUsername);
                            new GetNamesList(mFriendsData, mFriendsColumns, mFriendsListAdapter).execute(url);
                        } catch (Exception e) {
                            System.out.println(e.toString());
                        }
                    }
                });
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setContentView(R.layout.activity_main);

        this.initUsername();
        this.refreshMapButton();

        mFriendRequestsListAdapter = new SimpleAdapter(this, mFriendRequestsData,
                mFriendRequestLayout, mFriendRequestsColumns, mFriendRequestsTargetViews);

        mFriendsListAdapter = new SimpleAdapter(this, mFriendsData,
                mFriendsLayout, mFriendsColumns, mFriendsTargetViews);

        ((ListView) findViewById(R.id.RequestList)).setAdapter(mFriendRequestsListAdapter);
        ((ListView) findViewById(R.id.friends_list)).setAdapter(mFriendsListAdapter);

        this.initTabs();


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Thread(new UpdateTask(mHandler)).start();
    }

    private class GetNamesList extends AsyncTask<URL, Void, ArrayList<String>> {

        List<HashMap<String, String>> mData;
        String[] mColumnNames;
        SimpleAdapter mAdapter;

        GetNamesList (List<HashMap<String, String>> data, String[] columnNames, SimpleAdapter adapter) {
            mData = data;
            mColumnNames = columnNames;
            mAdapter = adapter;
        }

        protected ArrayList<String> doInBackground(URL... urls) {
            HttpURLConnection urlConnection = null;
            StringBuilder result = new StringBuilder();
            try {
                urlConnection = (HttpURLConnection) urls[0].openConnection();

                urlConnection.setRequestMethod("GET");

                InputStream response = urlConnection.getInputStream();
                BufferedReader responseReader = new BufferedReader(new InputStreamReader(response));

                String line;
                while ((line = responseReader.readLine()) != null)
                    result.append(line);


                System.out.println("Server reply: " + result.toString());
            } catch (Exception e) {
                System.out.println("Something went wrong when getting locations: " + e.toString());
            }

            ArrayList<String> resultList = new ArrayList<>();
            try {
                JSONArray friendsNames = new JSONArray(result.toString());

                for (int i = 0; i < friendsNames.length(); ++i)
                    resultList.add(friendsNames.get(i).toString());

            } catch (JSONException jsonEx) {
                System.out.println("JSON exception: " + jsonEx.toString());
            }

            return resultList;
        }

        protected void onPostExecute(ArrayList<String> friendsList) {
            // Change this, make it work better
            int oldSize = mData.size();

            mData.clear();
            for (int i = 0; i < friendsList.size(); ++i) {
                HashMap<String, String> entry = new HashMap<>();
                entry.put(mColumnNames[0], friendsList.get(i));
                mData.add(entry);
            }
            if (mData.size() != oldSize)
                mAdapter.notifyDataSetChanged();
        }
    }

    private void initFriendRequestsTab () {
        // Proceed with the request only if the username is set
        if (mUsername.length() == 0) return;

        try {
            URL url = new URL("http://37.139.6.168:3000/pending_requests?username=" + mUsername);
            new GetNamesList(mFriendRequestsData, mFriendRequestsColumns, mFriendRequestsListAdapter).execute(url);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private void initFriendsTab () {
        // Proceed with the request only if the username is set
        if (mUsername.length() == 0) return;

        try {
            URL url = new URL("http://37.139.6.168:3000/friends?username=" + mUsername);
            new GetNamesList(mFriendsData, mFriendsColumns, mFriendsListAdapter).execute(url);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    private void initTabs () {
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        // Main Tab
        TabHost.TabSpec spec = tabHost.newTabSpec("Home");
        spec.setContent(R.id.MainTab);
        spec.setIndicator("Home");
        tabHost.addTab(spec);

        // Friend Request Tab
        spec = tabHost.newTabSpec("Requests");
        spec.setContent(R.id.FriendsTab);
        spec.setIndicator("Requests");
        tabHost.addTab(spec);

        // Friends Tab
        spec = tabHost.newTabSpec("Friends");
        spec.setContent(R.id.friends_list_layout);
        spec.setIndicator("Friends");
        tabHost.addTab(spec);

        this.initFriendsTab();
        this.initFriendRequestsTab();
    }

    private void refreshUsername () {
        SharedPreferences preferences = getSharedPreferences("user_name", MODE_PRIVATE);
        String defaultUserName = getResources().getString(R.string.default_user_name);
        String userName = preferences.getString("username", defaultUserName);

        Button userNameBtn = (Button) (findViewById(R.id.button_send));
        EditText usernameField = (EditText) findViewById(R.id.text_input_username);

        if (!userName.equals(defaultUserName)) {
            mUsername = userName;

            usernameField.setText(userName);

            final RelativeLayout.LayoutParams fieldParams =
                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);

            fieldParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            fieldParams.addRule(RelativeLayout.CENTER_IN_PARENT);

            usernameField.setLayoutParams(fieldParams);

            userNameBtn.setEnabled(false);
            userNameBtn.setText("Change");
        } else {
            usernameField.setHint("Create account");

            userNameBtn.setEnabled(true);
            userNameBtn.setText("Create");
        }
    }

    private void initUsername () {

        ((EditText) this.findViewById(R.id.text_input_username)).addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                EditText editText = (EditText) findViewById(R.id.text_input_username);
                String username = editText.getText().toString();

                if (!username.contentEquals(mUsername)) {
                    Button userNameBtn = (Button) (findViewById(R.id.button_send));
                    userNameBtn.setEnabled(true);
                    userNameBtn.setClickable(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }


        });

        this.refreshUsername();
    }

    private class ChangeUsername extends AsyncTask<Void, Void, String> {

        String mNewUsername;
        boolean mChangeScenario;

        ChangeUsername (String newUsername, boolean change) {
            mNewUsername = newUsername;
            mChangeScenario = change;
        }

        @Override
        protected String doInBackground(Void... params) {
            HttpURLConnection connection = null;
            StringBuilder result = new StringBuilder();
            try {
                JSONObject postBody = new JSONObject();
                if (mChangeScenario) {
                    postBody.put("oldusername", mUsername);
                    postBody.put("newusername", mNewUsername);
                } else {
                    postBody.put("username", mNewUsername);
                }

                String urlString = mBaseURL + (mChangeScenario ? "change" : "register");

                connection = (HttpURLConnection) new URL(urlString).openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");

                OutputStream message = connection.getOutputStream();
                message.write(postBody.toString().getBytes());
                System.out.println("Create/Change message sent: " + postBody.toString());

                InputStream response = connection.getInputStream();
                BufferedReader responseReader = new BufferedReader(new InputStreamReader(response));

                String line;
                while ((line = responseReader.readLine()) != null)
                    result.append(line);

                System.out.println("Create/Change result: " + result.toString());
            } catch (Exception e) {
                System.out.println("Create/Change username exception: " + e.toString());
            }

            return result.toString();
        }

        @Override
        protected void onPostExecute(String serverResponse) {
            boolean success = serverResponse.equals("Username changed") || serverResponse.equals("OK");
            if (success) {
                mUsername = mNewUsername;

                SharedPreferences preferences = getSharedPreferences("user_name", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("username", mUsername);
                editor.commit();

                refreshUsername();
                refreshMapButton();
            } else if (serverResponse.equals("Username already exists")) {
                // Nothing, just show the message
            }
        }
    }

    public void onCreateButtonClicked(View view) {
        EditText editText = (EditText) findViewById(R.id.text_input_username);
        String newUsername = editText.getText().toString();

        // Only if the input string is not empty
        if (!newUsername.equals("")) {
            // Changing username, or registering for the first time?
            boolean change = !mUsername.equals("");
            new ChangeUsername(newUsername, change).execute();
        }
    }

    private void refreshMapButton() {
        Button mapButton = (Button) findViewById(R.id.button_map);

        if(mUsername.equals("")) {
            mapButton.setEnabled(false);
            mapButton.setClickable(false);
        } else {
            mapButton.setEnabled(true);
            mapButton.setClickable(true);
        }
    }

    public void onMapButtonClicked(View view) {
        // If the map button is clickable, then it already has a username
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    private class SendUserAction extends AsyncTask<String, Void, Void> {

        private String mFriendName;

        SendUserAction(String friendName) { mFriendName = friendName; }

        @Override
        protected Void doInBackground(String... params) {
            String route = params[0];
            HttpURLConnection connection = null;

            try {
                JSONObject body = new JSONObject();
                body.put("username", mUsername);
                body.put("friend", mFriendName);

                connection = (HttpURLConnection) new URL(mBaseURL + route).openConnection();
                connection.setRequestMethod("PUT");
                connection.setRequestProperty("Content-Type", "application/json");

                OutputStream message = connection.getOutputStream();
                message.write(body.toString().getBytes());
                System.out.println("SendUserAction message sent: " + body.toString());

                InputStream response = connection.getInputStream();
                BufferedReader responseReader = new BufferedReader(new InputStreamReader(response));

                StringBuilder result = new StringBuilder();
                String line;
                while ((line = responseReader.readLine()) != null)
                    result.append(line);

                System.out.println("SendUserAction result: " + result.toString());
            } catch (Exception ex) {
                System.out.println("SendUserAction exception: " + ex.toString());
            }

            return null;
        }
    }

    public void onAcceptButtonClicked(View view) {
        // Find Button's parent, then look for the TextView holding the
        // name of the friend to be accepted
        View parent = (View) view.getParent();
        TextView nameTextView = (TextView) parent.findViewById(R.id.friend_name);
        String friendName = nameTextView.getText().toString();
        new SendUserAction(friendName).execute("accept");
    }

    public void onRejectButtonClicked(View view) {
        // Find Button's parent, then look for the TextView holding the
        // name of the friend to be rejected
        View parent = (View) view.getParent();
        TextView nameTextView = (TextView) parent.findViewById(R.id.friend_name);
        String friendName = nameTextView.getText().toString();
        new SendUserAction(friendName).execute("reject");
    }

    public void onAddButtonClicked(View view) {
        // Find Button's parent, then look for the TextView holding the
        // name of the friend to be added
        View parent = (View) view.getParent();
        TextView nameTextView = (TextView) parent.findViewById(R.id.add_friend_input);
        String friendName = nameTextView.getText().toString();
        new SendUserAction(friendName).execute("add");
    }

    public void onRemoveButtonClicked(View view) {
        // Find Button's parent, then look for the TextView holding the
        // name of the friend to be removed
        View parent = (View) view.getParent();
        TextView nameTextView = (TextView) parent.findViewById(R.id.existing_friend_name);
        String friendName = nameTextView.getText().toString();
        new SendUserAction(friendName).execute("remove");
    }
}

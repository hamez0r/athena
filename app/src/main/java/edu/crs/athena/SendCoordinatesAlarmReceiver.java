package edu.crs.athena;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class SendCoordinatesAlarmReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 1;
    public static final String ACTION = "ALARM";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("AlarmReceiver", "Alarm received!");

        Intent i = new Intent(context, SendCoordinatesService.class);
        context.startService(i);
    }
}
